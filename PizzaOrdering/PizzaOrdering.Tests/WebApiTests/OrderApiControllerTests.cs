﻿using System.Linq;
using NUnit.Framework;
using PizzaOrdering.Infractructure.Model.Data;
using PizzaOrdering.Tests.UnitTestBase;
using TestBase;

namespace PizzaOrdering.Tests.WebApiTests
{
    [Category("WebApi")]
    public class OrderApiControllerTests : WebApiTestBase
    {
        private const string API = "OrderApi";

        [Test]
        [Description("Тест на получение списка заказов")]
        public void GetOrdersTest()
        {
            AssertAreEqualsOrdersLists(
                ExpectedOrders,
                _requestSender.SendRequest<OrderDomain>(API));
        }

        [Test]
        [Description("Тест на получение заказа по id")]
        public void GetOrderTest()
        {
            foreach (var expectedOrder in ExpectedOrders)
            {
                var actualOrder = _requestSender
                    .SendRequest<OrderDomain>(API, id: expectedOrder.Id)
                    .First();

                CompareOrders(
                    expectedOrder,
                    actualOrder)
                    .ShouldBeTrue();
            }
        }

        [Test]
        [Description("Тест на создание заказа")]
        public void CreateAndDeleteOrderTest()
        {
            ExpectedOrders.Add(NewOrder);
            var actualOrders = _requestSender
                .SendRequest(API, "post", DataConverter.ConvertDomainToContainer(NewOrder))
                .Select(DataConverter.ConvertContainerToDomain)
                .ToList();

            AssertAreEqualsOrdersLists(
                ExpectedOrders,
                actualOrders);

            ExpectedOrders.Remove(NewOrder);
            actualOrders = _requestSender
                .SendRequest<OrderDomain>(API, "delete", id: NewOrder.Id)
                .ToList();

            AssertAreEqualsOrdersLists(
                ExpectedOrders,
                actualOrders);
        }

        [Test]
        [Description("Тест на изменение заказа")]
        public void UpdateOrderTest()
        {
            var orderToChange = ExpectedOrders[0];
            NewOrder.Id = orderToChange.Id;
            ExpectedOrders[0] = NewOrder;
            var actualOrders = _requestSender
                .SendRequest(API, "put", DataConverter.ConvertDomainToContainer(ExpectedOrders[0]))
                .Select(DataConverter.ConvertContainerToDomain)
                .ToList();

            AssertAreEqualsOrdersLists(
                ExpectedOrders,
                actualOrders);

            ExpectedOrders[0] = orderToChange;
            actualOrders = _requestSender
                .SendRequest(API, "put", DataConverter.ConvertDomainToContainer(ExpectedOrders[0]))
                .Select(DataConverter.ConvertContainerToDomain)
                .ToList();

            AssertAreEqualsOrdersLists(
                ExpectedOrders,
                actualOrders);
        }

        [Test]
        [Ignore("Functional is not research!")]
        [Description("тест на валидацию заказа перед созданием")]
        public void ValidateBeforeCreateOrderTest()
        {
        }

        [Test]
        [Ignore("Functional is not research!")]
        [Description("тест на валидацию заказа перед изменением")]
        public void ValidateBeforeUpdateOrderTest()
        {
        }
    }
}