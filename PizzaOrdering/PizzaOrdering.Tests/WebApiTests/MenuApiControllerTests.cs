﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using NUnit.Framework;
using PizzaOrdering.Infractructure.Model.Data;
using TestBase;

namespace PizzaOrdering.Tests.WebApiTests
{
    [Category("WebApi")]
    public class MenuApiControllerTests : WebApiTestBase
    {
        private const string API = "MenuApi";

        [Test]
        [Description("Тест на отправку меню по Api")]
        public void GetMenuTest()
        {
            for (var j = 0; j < 2; j++)
            {
                var actualMenuPositions = _requestSender.SendRequest<MenuPositionDomain>(API);

                actualMenuPositions
                    .ShouldBeEqualCount(ExpectedMenuPositions.Count);

                for (var n = 0; n < ExpectedMenuPositions.Count; n++)
                {
                    CompareMenuPositions(actualMenuPositions[n], ExpectedMenuPositions[n])
                        .ShouldBeTrue();
                }

                if (j > 0) break;

                ExpectedMenuPositions.Add(NewMenuPosition);
                DbContextMenuSet.AddOrUpdate(NewMenuPosition);
                DbContext.SaveChanges();
            }
            ExpectedMenuPositions.Remove(NewMenuPosition);
        }

        [Test]
        [Description("Тест на отправку меню по Api")]
        public void GetMenuPositionTest()
        {
            for (var j = 0; j < 2; j++)
            {
                foreach (var expectedPosition in ExpectedMenuPositions)
                {
                    var actualMenuPosition = _requestSender
                        .SendRequest<MenuPositionDomain>(API, id: expectedPosition.Id)
                        .First();
                    CompareMenuPositions(actualMenuPosition, expectedPosition)
                        .ShouldBeTrue();
                }

                if (j > 0) break;

                ExpectedMenuPositions.Add(NewMenuPosition);
                DbContextMenuSet.AddOrUpdate(NewMenuPosition);
                DbContext.SaveChanges();
            }
            ExpectedMenuPositions.Remove(NewMenuPosition);
        }
    }
}