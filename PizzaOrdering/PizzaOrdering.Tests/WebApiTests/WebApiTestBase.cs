﻿using System.Collections.Generic;
using System.Configuration;
using WebApiClientBase;

namespace PizzaOrdering.Tests.WebApiTests
{
    public abstract class WebApiTestBase : UnitTestBase.UnitTestBase
    {
        protected readonly RequestSender _requestSender = new RequestSender();

        protected class RequestSender : ClientServiceBase
        {
            private string BaseUrl => ConfigurationManager.AppSettings["baseUrl"];

            public List<T> SendRequest<T>(string api, string type = "get", T body = null, int? id = null) where T : class
            {
                var url = new WebApiUrl(BaseUrl, api);
                switch (type.ToLower())
                {
                    case "post":
                        return Post<List<T>>(url, body);
                    case "delete":
                        return Delete<List<T>>($"{url}{id}");
                    case "put":
                        return Put<List<T>>(url, body);
                    default:
                        return Get<List<T>>($"{url}{id}");
                }
            }
        }
    }
}