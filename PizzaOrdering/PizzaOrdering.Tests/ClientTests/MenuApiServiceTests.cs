﻿using System.Data.Entity.Migrations;
using NUnit.Framework;
using PizzaOrdering.Tests.UnitTestBase;
using TestBase;

namespace PizzaOrdering.Tests.ClientTests
{
    [Category("Client")]
    public class MenuApiServiceTests : UnitTestBase.UnitTestBase
    {
        [Test]
        [Description("Тест на получение меню по Api")]
        public void GetMenuTest()
        {
            for (var j = 0; j < 2; j++)
            {
                var actualMenuPositions =
                    MenuApiService.GetMenu();

                actualMenuPositions
                    .ShouldBeEqualCount(ExpectedMenuPositions.Count);

                for (var n = 0; n < ExpectedMenuPositions.Count; n++)
                {
                    CompareMenuPositions(
                        DataConverter.ConvertContainerToDomain(actualMenuPositions[n]),
                        ExpectedMenuPositions[n])
                        .ShouldBeTrue();
                }

                if (j > 0) break;

                ExpectedMenuPositions.Add(NewMenuPosition);
                DbContextMenuSet.AddOrUpdate(NewMenuPosition);
                DbContext.SaveChanges();
            }
            ExpectedMenuPositions.Remove(NewMenuPosition);
        }

        [Test]
        [Description("Тест на получение позиции меню по Id по Api")]
        public void GetMenuPositionTest()
        {
            for (var j = 0; j < 2; j++)
            {
                foreach (var expectedMenuPosition in ExpectedMenuPositions)
                {
                    var actualMenuPosition = MenuApiService.GetMenuPosition(expectedMenuPosition.Id);
                    CompareMenuPositions(DataConverter.ConvertContainerToDomain(actualMenuPosition), expectedMenuPosition)
                        .ShouldBeTrue();
                }

                if (j > 0) break;

                ExpectedMenuPositions.Add(NewMenuPosition);
                DbContextMenuSet.AddOrUpdate(NewMenuPosition);
                DbContext.SaveChanges();
            }
            ExpectedMenuPositions.Remove(NewMenuPosition);
        }
    }
}