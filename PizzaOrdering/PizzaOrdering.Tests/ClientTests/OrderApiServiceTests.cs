﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using PizzaOrdering.Client.Model;
using PizzaOrdering.Tests.UnitTestBase;
using TestBase;

namespace PizzaOrdering.Tests.ClientTests
{
    [Category("Client")]
    public class OrderApiServiceTests : UnitTestBase.UnitTestBase
    {
        [Test]
        [Description("Тест на получение списка заказов")]
        public void GetOrdersTest()
        {
            AssertAreEqualsOrdersLists(
                ExpectedOrders,
                OrderApiService
                    .GetOrders()
                    .Select(DataConverter.ConvertContainerToDomain)
                    .ToList());
        }

        [Test]
        [Description("Тест на получение заказа по id")]
        public void GetOrderTest()
        {
            OrderApiService.GetOrders()
                .ShouldBeEqualCount(ExpectedOrders.Count);

            foreach (var expectedOrder in ExpectedOrders)
            {
                var actualOrder = OrderApiService.GetOrder(expectedOrder.Id);
                CompareOrders(
                    expectedOrder,
                    DataConverter.ConvertContainerToDomain(actualOrder))
                    .ShouldBeTrue();
            }
        }

        [Test]
        [Description("Тест на создание заказа")]
        public void CreateAndDeleteOrderTest()
        {
            ExpectedOrders.Add(NewOrder);
            var actualOrders = OrderApiService
                .CreateOrder(DataConverter.ConvertDomainToContainer(NewOrder))
                .Select(DataConverter.ConvertContainerToDomain)
                .ToList();

            AssertAreEqualsOrdersLists(
                ExpectedOrders,
                actualOrders);

            ExpectedOrders.Remove(NewOrder);
            actualOrders = OrderApiService
                .CancelOrder(actualOrders
                    .First(order => order.FirstName == NewOrder.FirstName).Id)
                .Select(DataConverter.ConvertContainerToDomain)
                .ToList();

            AssertAreEqualsOrdersLists(
                ExpectedOrders,
                actualOrders);
        }

        [Test]
        [Description("Тест на изменение заказа")]
        public void UpdateOrderTest()
        {
            var orderToChange = ExpectedOrders[0];
            NewOrder.Id = orderToChange.Id;
            ExpectedOrders[0] = NewOrder;
            var actualOrders = OrderApiService
                .UpdateOrder(DataConverter.ConvertDomainToContainer(ExpectedOrders[0]))
                .Select(DataConverter.ConvertContainerToDomain)
                .ToList();

            AssertAreEqualsOrdersLists(
                ExpectedOrders,
                actualOrders);

            ExpectedOrders[0] = orderToChange;
            actualOrders = OrderApiService
                .UpdateOrder(DataConverter.ConvertDomainToContainer(ExpectedOrders[0]))
                .Select(DataConverter.ConvertContainerToDomain)
                .ToList();

            AssertAreEqualsOrdersLists(
                ExpectedOrders,
                actualOrders);
        }

        [Test]
        [Description("тест на валидацию заказа перед созданием")]
        public void ValidateBeforeCreateOrderTest()
        {
            var newOrder = new OrderContainer();
            try
            {
                OrderApiService.CreateOrder(newOrder);
            }
            catch (Exception e)
            {
                e.Message.EndsWith("OrderPositions Street House Flat")
                    .ShouldBeTrue();
            }

            newOrder.Flat = "1";
            try
            {
                OrderApiService.CreateOrder(newOrder);
            }
            catch (Exception e)
            {
                e.Message.EndsWith("OrderPositions Street House")
                    .ShouldBeTrue();
            }

            newOrder.House = "1";
            try
            {
                OrderApiService.CreateOrder(newOrder);
            }
            catch (Exception e)
            {
                e.Message.EndsWith("OrderPositions Street")
                    .ShouldBeTrue();
            }

            newOrder.Street = "1";
            try
            {
                OrderApiService.CreateOrder(newOrder);
            }
            catch (Exception e)
            {
                e.Message.EndsWith("OrderPositions")
                    .ShouldBeTrue();
            }

            newOrder.Street = "";
            newOrder.OrderPositions = new List<OrderPositionContainer>
            {
                new OrderPositionContainer
                {
                    MenuPositionId = 1,
                    Count = 1
                }
            };
            try
            {
                OrderApiService.CreateOrder(newOrder);
            }
            catch (Exception e)
            {
                e.Message.EndsWith("Street")
                    .ShouldBeTrue();
            }
        }

        [Test]
        [Description("тест на валидацию заказа перед изменением")]
        public void ValidateBeforeUpdateOrderTest()
        {
            var newOrder = new OrderContainer();
            try
            {
                OrderApiService.UpdateOrder(newOrder);
            }
            catch (Exception e)
            {
                e.Message.EndsWith("OrderPositions Street House Flat")
                    .ShouldBeTrue();
            }

            newOrder.Flat = "1";
            try
            {
                OrderApiService.UpdateOrder(newOrder);
            }
            catch (Exception e)
            {
                e.Message.EndsWith("OrderPositions Street House")
                    .ShouldBeTrue();
            }

            newOrder.House = "1";
            try
            {
                OrderApiService.UpdateOrder(newOrder);
            }
            catch (Exception e)
            {
                e.Message.EndsWith("OrderPositions Street")
                    .ShouldBeTrue();
            }

            newOrder.Street = "1";
            try
            {
                OrderApiService.UpdateOrder(newOrder);
            }
            catch (Exception e)
            {
                e.Message.EndsWith("OrderPositions")
                    .ShouldBeTrue();
            }

            newOrder.Street = "";
            newOrder.OrderPositions = new List<OrderPositionContainer>
            {
                new OrderPositionContainer
                {
                    MenuPositionId = 1,
                    Count = 1
                }
            };
            try
            {
                OrderApiService.UpdateOrder(newOrder);
            }
            catch (Exception e)
            {
                e.Message.EndsWith("Street")
                    .ShouldBeTrue();
            }
        }
    }
}