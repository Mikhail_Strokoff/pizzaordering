﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Autofac;
using AutofacBase;
using InfrastructureBase.Data;
using NUnit.Framework;
using PizzaOrdering.Client.AutofacModules;
using PizzaOrdering.Client.Services.Interfaces;
using PizzaOrdering.Infractructure.AutofacModules;
using PizzaOrdering.Infractructure.Migrations;
using PizzaOrdering.Infractructure.Model;
using PizzaOrdering.Infractructure.Model.Data;
using PizzaOrdering.Infractructure.Services.Interfaces;
using TestBase;

namespace PizzaOrdering.Tests.UnitTestBase
{
    [TestFixture]
    public abstract class UnitTestBase
    {
        protected IContainer Container;

        protected PizzaDbContext DbContext;

        protected DbSet<MenuPositionDomain> DbContextMenuSet;
        protected DbSet<OrderPositionDomain> DbContextOrderPositionSet;
        protected DbSet<OrderDomain> DbContextOrderSet;

        protected List<MenuPositionDomain> ExpectedMenuPositions;
        protected List<OrderPositionDomain> ExpectedOrderPositions;
        protected List<OrderDomain> ExpectedOrders;

        protected IMenuApiService MenuApiService;
        protected IRepository<MenuPositionDomain> MenuPositionRepository;
        protected IMenuPositionService MenuPositionService;

        protected MenuPositionDomain NewMenuPosition;
        protected OrderDomain NewOrder;

        protected IOrderApiService OrderApiService;
        protected IRepository<OrderDomain> OrderRepository;
        protected IOrderService OrderService;

        [TestFixtureSetUp]
        public virtual void TestFixtureSetUp()
        {
            var containerBuilder = new ContainerBuilder();
            new ClientModuleBuilder(containerBuilder).Registers(ModuleType.SingleInstance);
            new PizzaOrderingInfrastructureModuleBuilder(containerBuilder).Registers(ModuleType.SingleInstance);
            Container = containerBuilder.Build();

            #region Resolves

            DbContext = Container.Resolve<IDbContext>() as PizzaDbContext ?? new PizzaDbContext();

            MenuPositionRepository = Container.Resolve<IRepository<MenuPositionDomain>>();
            OrderRepository = Container.Resolve<IRepository<OrderDomain>>();

            MenuPositionService = Container.Resolve<IMenuPositionService>();
            OrderService = Container.Resolve<IOrderService>();

            MenuApiService = Container.Resolve<IMenuApiService>();
            OrderApiService = Container.Resolve<IOrderApiService>();

            DbContextMenuSet = DbContext.Set<MenuPositionDomain>();
            DbContextOrderSet = DbContext.Set<OrderDomain>();
            DbContextOrderPositionSet = DbContext.Set<OrderPositionDomain>();

            #endregion

            #region Data

            #region MenuPositions

            ExpectedMenuPositions = new List<MenuPositionDomain>
            {
                new MenuPositionDomain
                {
                    Id = 1,
                    Name = "Салями",
                    Description = "Классическая итальянская пицца с салями",
                    Price = 653.5
                },
                new MenuPositionDomain
                {
                    Id = 2,
                    Name = "Чили",
                    Description = "Остренькая и вкусная пицца специально для гурманов!",
                    Price = 517
                },
                new MenuPositionDomain
                {
                    Id = 3,
                    Name = "Баварская",
                    Description = "Это что-то с чем-то!",
                    Price = 587
                }
            };

            #endregion

            #region Orders

            ExpectedOrders = new List<OrderDomain>
            {
                new OrderDomain
                {
                    Id = 1,
                    FirstName = "Вася",
                    LastName = "Смирнов",
                    Street = "Ленина",
                    House = "12",
                    Entrance = 1,
                    Flat = "6"
                },
                new OrderDomain
                {
                    Id = 2,
                    FirstName = "Пётр",
                    LastName = "Емельянов",
                    Street = "Саранская",
                    House = "34",
                    Entrance = 2,
                    Flat = "24к"
                },
                new OrderDomain
                {
                    Id = 3,
                    FirstName = "Иван",
                    LastName = "Кутузов",
                    Street = "Смирных",
                    House = "153",
                    Entrance = 7,
                    Flat = "139"
                },
                new OrderDomain
                {
                    Id = 4,
                    FirstName = "Александр",
                    LastName = "Андриевских",
                    Street = "Татищева",
                    House = "2а",
                    Entrance = 4,
                    Flat = "76"
                }
            };

            #endregion

            #region OrderPositions

            ExpectedOrderPositions = new List<OrderPositionDomain>
            {
                new OrderPositionDomain
                {
                    Id = 1,
                    MenuPositionId = 1,
                    OrderId = 1,
                    Count = 1
                },
                new OrderPositionDomain
                {
                    Id = 2,
                    MenuPositionId = 2,
                    OrderId = 1,
                    Count = 2
                },
                new OrderPositionDomain
                {
                    Id = 3,
                    MenuPositionId = 1,
                    OrderId = 2,
                    Count = 7
                },
                new OrderPositionDomain
                {
                    Id = 4,
                    MenuPositionId = 2,
                    OrderId = 2,
                    Count = 3
                },
                new OrderPositionDomain
                {
                    Id = 5,
                    MenuPositionId = 3,
                    OrderId = 2,
                    Count = 5
                },
                new OrderPositionDomain
                {
                    Id = 6,
                    MenuPositionId = 3,
                    OrderId = 3,
                    Count = 1
                },
                new OrderPositionDomain
                {
                    Id = 7,
                    MenuPositionId = 2,
                    OrderId = 4,
                    Count = 3
                },
                new OrderPositionDomain
                {
                    Id = 8,
                    MenuPositionId = 3,
                    OrderId = 4,
                    Count = 1
                }
            };

            #endregion

            #region New Data

            NewOrder = new OrderDomain
            {
                Id = 5,
                FirstName = "Порфирий",
                LastName = "Кутейкин",
                Street = "Аванграда",
                House = "1",
                Entrance = 1,
                Flat = "1",
                OrderPositions = new List<OrderPositionDomain>
                {
                    new OrderPositionDomain
                    {
                        Id = 9,
                        MenuPositionId = ExpectedMenuPositions[0].Id,
                        MenuPositionDomain = ExpectedMenuPositions[0],
                        Count = 1
                    },
                    new OrderPositionDomain
                    {
                        Id = 10,
                        MenuPositionId = ExpectedMenuPositions[1].Id,
                        MenuPositionDomain = ExpectedMenuPositions[1],
                        Count = 2
                    }
                }
            };

            NewMenuPosition = new MenuPositionDomain
            {
                Id = 4,
                Name = "Кактус-лайм",
                Description = "Очень странная, но освежающая пицца с кактусом и лаймом",
                Price = 850.7
            };

            #endregion

            #endregion
        }

        [TestFixtureTearDown]
        public virtual void TestFixtureTearDown()
        {
            Container.Dispose();
        }

        [SetUp]
        public virtual void SetUp()
        {
            ClearDatabase();

            DbContextMenuSet.AddOrUpdate(ExpectedMenuPositions.ToArray());
            DbContextOrderSet.AddOrUpdate(ExpectedOrders.ToArray());
            DbContextOrderPositionSet.AddOrUpdate(ExpectedOrderPositions.ToArray());

            DbContext.SaveChanges();
        }

        protected bool CompareMenuPositions(MenuPositionDomain left, MenuPositionDomain right)
        {
            //Console.WriteLine(
            //    $"{left.Id}, {right.Id} \r\n" +
            //    $"{left.Name}, {right.Name} \r\n" +
            //    $"{left.Description}, {right.Description} \r\n" +
            //    $"{left.Price}, {right.Price} \r\n"
            //    );
            return
                Equals(left.Id, right.Id) &&
                Equals(left.Name, right.Name) &&
                Equals(left.Description, right.Description) &&
                Equals(left.Price, right.Price);
        }

        protected bool CompareOrders(OrderDomain left, OrderDomain right)
        {
            //Console.WriteLine($"{left.Id} || {left.FirstName} || {left.LastName}");
            //Console.WriteLine($"{right.Id} || {right.FirstName} || {right.LastName}");
            //Console.WriteLine();
            //Console.WriteLine($"{left.Street} || {left.House} || {left.Flat} || {left.Entrance}");
            //Console.WriteLine($"{right.Street} || {right.House} || {right.Flat} || {right.Entrance}");
            //Console.WriteLine();
            //Console.WriteLine();
            return
                Equals(left.Id, right.Id) &&
                Equals(left.FirstName, right.FirstName) &&
                Equals(left.LastName, right.LastName) &&
                Equals(left.Street, right.Street) &&
                Equals(left.House, right.House) &&
                Equals(left.Flat, right.Flat) &&
                Equals(left.Entrance, right.Entrance) &&
                ComparePositionLists(
                    left.OrderPositions.ToList(),
                    right.OrderPositions.ToList());
        }

        protected void AssertAreEqualsOrdersLists(List<OrderDomain> left, List<OrderDomain> right)
        {
            right.ShouldBeEqualCount(left.Count);

            for (var i = 0; i < left.Count; i++)
            {
                CompareOrders(left[i], right[i])
                    .ShouldBeTrue();
            }
        }

        protected bool CompareOrderPositions(OrderPositionDomain left, OrderPositionDomain right)
        {
            //Console.WriteLine(" --- position: --- ");
            //Console.WriteLine($"{left.Id} || {left.OrderId} || {left.MenuPositionId} || {left.Count}");
            //Console.WriteLine($"{right.Id} || {right.OrderId} || {right.MenuPositionId} || {right.Count}");
            //Console.WriteLine(" ----------------- ");
            return
                Equals(left.MenuPositionId, right.MenuPositionId) &&
                Equals(left.Count, right.Count);
        }

        protected bool ComparePositionLists(List<OrderPositionDomain> left, List<OrderPositionDomain> right)
        {
            //Console.WriteLine($"{left.Count} == {right.Count}");
            return
                left.Count == right.Count &&
                !left.Where((t, i) =>
                    !CompareOrderPositions(t, right[i]))
                    .Any();
        }

        protected void ClearDatabase()
        {
            /*var result = */DbContext.Database.ExecuteSqlCommand(
                "drop table dbo.__MigrationHistory " +
                "drop table dbo.pizza_order_position " +
                "drop table dbo.pizza_order " +
                "drop table dbo.pizza_menu_position ");
            DbContext.SaveChanges();

            new Configuration().Update();

            //Console.WriteLine($"Database cleaned, result: {result}");
        }
    }
}