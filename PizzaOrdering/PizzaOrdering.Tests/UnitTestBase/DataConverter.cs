﻿using System.Linq;
using PizzaOrdering.Client.Model;
using PizzaOrdering.Infractructure.Model.Data;

namespace PizzaOrdering.Tests.UnitTestBase
{
    public abstract class DataConverter
    {
        public static MenuPositionDomain ConvertContainerToDomain(MenuPositionContainer container)
        {
            return new MenuPositionDomain
            {
                Id = container.Id,
                Name = container.Name,
                Description = container.Description,
                Price = container.Price
            };
        }

        public static OrderDomain ConvertContainerToDomain(OrderContainer container)
        {
            return new OrderDomain
            {
                Id = container.Id,
                FirstName = container.FirstName,
                LastName = container.LastName,
                Street = container.Street,
                House = container.House,
                Flat = container.Flat,
                Entrance = container.Entrance,
                OrderPositions = container.OrderPositions
                    .Select(ConvertContainerToDomain)
                    .ToList()
            };
        }

        public static OrderPositionDomain ConvertContainerToDomain(OrderPositionContainer container)
        {
            return new OrderPositionDomain
            {
                Id = container.Id,
                MenuPositionId = container.MenuPositionId,
                Count = container.Count,
                MenuPositionDomain = ConvertContainerToDomain(container.MenuPosition)
            };
        }

        public static MenuPositionContainer ConvertDomainToContainer(MenuPositionDomain domain)
        {
            return new MenuPositionContainer
            {
                Id = domain.Id,
                Name = domain.Name,
                Description = domain.Description,
                Price = domain.Price
            };
        }

        public static OrderContainer ConvertDomainToContainer(OrderDomain domain)
        {
            return new OrderContainer
            {
                Id = domain.Id,
                FirstName = domain.FirstName,
                LastName = domain.LastName,
                Street = domain.Street,
                House = domain.House,
                Flat = domain.Flat,
                Entrance = domain.Entrance,
                OrderPositions = domain.OrderPositions
                    .Select(ConvertDomainToContainer)
                    .ToList()
            };
        }

        public static OrderPositionContainer ConvertDomainToContainer(OrderPositionDomain domain)
        {
            return new OrderPositionContainer
            {
                Id = domain.Id,
                MenuPositionId = domain.MenuPositionId,
                Count = domain.Count,
                MenuPosition = ConvertDomainToContainer(domain.MenuPositionDomain)
            };
        }
    }
}