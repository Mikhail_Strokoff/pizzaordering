﻿using System.Data.Entity.Migrations;
using NUnit.Framework;
using TestBase;

namespace PizzaOrdering.Tests.InfrastructureTests
{
    [Category("Infrastructure")]
    public class MenuPositionServiceTests : UnitTestBase.UnitTestBase
    {
        [Test]
        [Description("Тест на получение меню")]
        public void GetMenuTest()
        {
            for (var j = 0; j < 2; j++)
            {
                var actualMenuPositions =
                    MenuPositionService.GetMenu();

                actualMenuPositions
                    .ShouldBeEqualCount(ExpectedMenuPositions.Count);

                for (var i = 0; i < ExpectedMenuPositions.Count; i++)
                {
                    CompareMenuPositions(actualMenuPositions[i], ExpectedMenuPositions[i])
                        .ShouldBeTrue();
                }

                if (j > 0) break;

                ExpectedMenuPositions.Add(NewMenuPosition);
                DbContextMenuSet.AddOrUpdate(NewMenuPosition);
                DbContext.SaveChanges();
            }
            ExpectedMenuPositions.Remove(NewMenuPosition);
        }

        [Test]
        [Description("Тест на получение позиции меню по Id")]
        public void GetMenuPositionTest()
        {
            for (var j = 0; j < 2; j++)
            {
                foreach (var expectedMenuPosition in ExpectedMenuPositions)
                {
                    var actualMenuPosition = MenuPositionService.GetMenuPosition(expectedMenuPosition.Id);
                    CompareMenuPositions(actualMenuPosition, expectedMenuPosition)
                        .ShouldBeTrue();
                }

                if (j > 0) break;

                ExpectedMenuPositions.Add(NewMenuPosition);
                DbContextMenuSet.AddOrUpdate(NewMenuPosition);
                DbContext.SaveChanges();
            }
            ExpectedMenuPositions.Remove(NewMenuPosition);
        }
    }
}