﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using PizzaOrdering.Infractructure.Model.Data;
using PizzaOrdering.Infractructure.Model.DataContainer;
using TestBase;

namespace PizzaOrdering.Tests.InfrastructureTests
{
    [Category("Infrastructure")]
    public class OrderServiceTests : UnitTestBase.UnitTestBase
    {
        [Test]
        [Description("Тест на получение списка заказов")]
        public
        void GetOrdersTest()
        {
            AssertAreEqualsOrdersLists(ExpectedOrders, OrderService.GetOrders());
        }

        [Test]
        [Description("Тест на получение заказа по id")]
        public
        void GetOrderTest()
        {
            OrderService.GetOrders()
                .ShouldBeEqualCount(ExpectedOrders.Count);

            foreach (var expectedOrder in ExpectedOrders)
            {
                var actualOrder = OrderService.GetOrderById(expectedOrder.Id);
                CompareOrders(expectedOrder, actualOrder)
                    .ShouldBeTrue();
            }
        }

        [Test]
        [Description("Тест на создание заказа")]
        public
        void CreateOrderTest()
        {
            var newOrder = new OrderDomain
            {
                Id = 5,
                FirstName = "Порфирий",
                LastName = "Кутейкин",
                Street = "Аванграда",
                House = "1",
                Entrance = 1,
                Flat = "1",
                OrderPositions = new List<OrderPositionDomain>
                {
                    new OrderPositionDomain
                    {
                        Id = 9,
                        MenuPositionId = ExpectedMenuPositions[0].Id,
                        MenuPositionDomain = ExpectedMenuPositions[0], //до записи в базу 
                        Count = 1
                    },
                    new OrderPositionDomain
                    {
                        Id = 10,
                        MenuPositionId = ExpectedMenuPositions[1].Id,
                        MenuPositionDomain = ExpectedMenuPositions[1],
                        Count = 2
                    }
                }
            };
            ExpectedOrders.Add(newOrder);
            OrderService.CreateOrder(new OrderContainer(newOrder));

            AssertAreEqualsOrdersLists(ExpectedOrders, OrderService.GetOrders());
            ExpectedOrders.Remove(newOrder);
        }

        [Test]
        [Description("Тест на изменение заказа")]
        public
        void UpdateOrderTest()
        {
            ExpectedOrders[0].Entrance += 1;
            ExpectedOrders[0].FirstName = "Васисуалий Андреевич";
            ExpectedOrders[0].OrderPositions.Add(new OrderPositionDomain
            {
                MenuPositionId = 1,
                Count = 1
            });

            OrderService.GetOrders()
                .ShouldBeEqualCount(ExpectedOrders.Count);

            foreach (var expectedOrder in ExpectedOrders)
            {
                var actualOrder = OrderService.GetOrderById(expectedOrder.Id);
                CompareOrders(expectedOrder, actualOrder)
                    .ShouldBeTrue();
            }
        }

        [Test]
        [Description("тест на отмену заказа")]
        public
        void CancelOrderTest()
        {
            var idToRemove = ExpectedOrders.Select(item => item.Id).Max();
            OrderService.CancelOrder(idToRemove);

            AssertAreEqualsOrdersLists(
                ExpectedOrders.Where(order => order.Id != idToRemove).ToList(),
                OrderService.GetOrders());
        }

        [Test]
        [Ignore("Functional is not research!")]
        [Description("тест на валидацию заказа перед созданием")]
        public
        void ValidateBeforeCreateOrderTest()
        {
            #region Data

            #endregion

            #region Actions

            #endregion

            #region Assertion

            Assert.True(false, "Uncomplete test");

            #endregion
        }

        [Test]
        [Ignore("Functional is not research!")]
        [Description("тест на валидацию заказа перед изменением")]
        public
        void ValidateBeforeUpdateOrderTest()
        {
            #region Data

            #endregion

            #region Actions

            #endregion

            #region Assertion

            Assert.True(false, "Uncomplete test");

            #endregion
        }
    }
}