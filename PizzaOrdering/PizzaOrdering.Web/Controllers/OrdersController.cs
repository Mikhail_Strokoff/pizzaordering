﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using PizzaOrdering.Infractructure.Model.DataContainer;
using PizzaOrdering.Infractructure.Services.Interfaces;
using PizzaOrdering.Web.ViewModel;

namespace PizzaOrdering.Web.Controllers
{
    public class OrdersController : Controller
    {
        private readonly IMenuPositionService _menuPositionService;
        private readonly IOrderService _orderService;

        public OrdersController(
            IOrderService orderService,
            IMenuPositionService menuPositionService)
        {
            _orderService = orderService;
            _menuPositionService = menuPositionService;
        }

        public ActionResult OrderCreate()
        {
            return View(new OrderCreatePageViewModel());
        }

        public ActionResult OrderList()
        {
            return View(new OrderListPageViewModel(
                _orderService.GetOrders()
                    .Select(order => new OrderContainer(order))
                    .ToList())
                );
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult OrderList([Bind] OrderContainer order)
        {
            order.OrderPositions = ModifyPositions(order.OrderPositions);
            _orderService.CreateOrder(order);
            return RedirectToAction("OrderList");
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult OrderDelete([FromUri] int id)
        {
            _orderService.CancelOrder(id);
            return RedirectToAction("OrderList");
        }

        public ActionResult Order([FromUri] int id)
        {
            return View(
                new OrderPageViewModel(
                    new OrderContainer(
                        _orderService.GetOrderById(id)
                        )
                    )
                );
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult Order([FromUri] int id, [Bind] string address)
        {
            var orderToShow = new OrderContainer(_orderService.GetOrderById(id));

            address = address
                .Replace("ул.", "")
                .Replace("кв.", "")
                .Replace("д.", "")
                .Replace("п.", "")
                .Replace(".", "");
            var indexStart = 0;
            var addressParts = new string[4];
            for (var i = 0; i < 4 && indexStart < address.Length; i++)
            {
                var indexEnd = address.IndexOf(',', indexStart);
                indexEnd = indexEnd < 0 ? address.Length : indexEnd;
                addressParts[i] = address.Substring(indexStart, indexEnd - indexStart).Trim();
                indexStart = indexEnd + 1;
            }
            orderToShow.Street = addressParts[0];
            orderToShow.House = addressParts[1];
            orderToShow.Flat = addressParts[2];
            int.TryParse(addressParts[3], out indexStart);
            orderToShow.Entrance = indexStart;

            _orderService.UpdateOrder(orderToShow);

            return View(new OrderPageViewModel(orderToShow));
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult GeneratePositions([FromBody] List<OrderPositionContainer> positions)
        {
            return View(ModifyPositions(positions));
        }

        private List<OrderPositionContainer> ModifyPositions(List<OrderPositionContainer> positions)
        {
            positions?.ForEach(item =>
                item.MenuPosition = new MenuPositionContainer(
                    _menuPositionService
                        .GetMenuPosition(item.MenuPositionId)));
            return positions;
        }
    }
}