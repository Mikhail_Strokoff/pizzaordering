﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using PizzaOrdering.Infractructure.Model.DataContainer;
using PizzaOrdering.Infractructure.Services.Interfaces;

namespace PizzaOrdering.Web.Controllers.Api
{
    public class MenuApiController : ApiController
    {
        private readonly IMenuPositionService _menuPositionService;

        public MenuApiController(
            IMenuPositionService menuPositionService)
        {
            _menuPositionService = menuPositionService;
        }

        // GET: MenuApi
        public List<MenuPositionContainer> Get([FromUri] int? id = null)
        {
            return _menuPositionService
                .GetMenu()
                .Select(menuDomain => new MenuPositionContainer(menuDomain))
                .Where(item => id == null || (id == item.Id))
                .ToList();
        }
    }
}