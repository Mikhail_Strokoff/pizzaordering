﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using PizzaOrdering.Infractructure.Model.DataContainer;
using PizzaOrdering.Infractructure.Services.Interfaces;

namespace PizzaOrdering.Web.Controllers.Api
{
    public class OrderApiController : ApiController
    {
        private readonly IOrderService _orderService;

        public OrderApiController(
            IOrderService orderService)
        {
            _orderService = orderService;
        }

        // GET: Order
        public List<OrderContainer> Get([FromUri] int? id = null)
        {
            return _orderService
                .GetOrders()
                .Select(order => new OrderContainer(order))
                .Where(item => id == null || (id == item.Id))
                .ToList();
        }

        //POST: Order
        public List<OrderContainer> Post([FromBody] OrderContainer order)
        {
            _orderService.CreateOrder(order);
            return Get();
        }

        //PUT: Order
        public List<OrderContainer> Put([FromBody] OrderContainer order)
        {
            _orderService.UpdateOrder(order);
            return Get();
        }

        //DELETE: Order
        public List<OrderContainer> Delete([FromUri] int id)
        {
            _orderService.CancelOrder(id);
            return Get();
        }
    }
}