﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PizzaOrdering.Infractructure.Model.Data;
using PizzaOrdering.Infractructure.Model.DataContainer;
using PizzaOrdering.Infractructure.Services.Interfaces;
using PizzaOrdering.Web.ViewModel;

namespace PizzaOrdering.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMenuPositionService _menuPositionService;

        public HomeController(IMenuPositionService menuPositionService)
        {
            _menuPositionService = menuPositionService;
        }

        private IEnumerable<MenuPositionDomain> MenuPositionDomains => _menuPositionService.GetMenu();

        public ActionResult Index()
        {
            var mainPageViewModel = new IndexPageViewModel(
                MenuPositionDomains.Select(pos => new MenuPositionContainer(pos)).ToList()
                );
            return View(mainPageViewModel);
        }
    }
}