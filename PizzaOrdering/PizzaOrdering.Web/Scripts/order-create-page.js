﻿function orderCreatePostRequest() {
    $($loader).removeAttr("hidden");
    var order = {};
    order.firstName = $("[name='firstName']").val();
    order.lastName = $("[name='lastName']").val();
    order.street = $("[name='street']").val();
    order.house = $("[name='house']").val();
    order.entrance = $("[name='entrance']").val();
    order.flat = $("[name='flat']").val();
    order.orderPositions = getOrderPositions();

    jQuery.post(
        $orderListPage,
        order,
        function () {
            sessionStorage.removeItem($positionsLitteral);
            location.href = $orderListPage;
        });
    $($loader).attr("hidden", true);
}

function generatePositions() {
    $($loader).removeAttr("hidden");

    $("#left-block").load("/Orders/GeneratePositions", { positions: getOrderPositions() });

    $($loader).attr("hidden", true);
}