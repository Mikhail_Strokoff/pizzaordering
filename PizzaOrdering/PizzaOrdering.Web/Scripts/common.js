﻿$(document).ready(page_onload);

$currentPageLitteral = "currentPage";
$positionsLitteral = "positions";
$mainPage = "/Home/Index";
$orderCreatePage = "/Orders/OrderCreate";
$orderListPage = "/Orders/OrderList";
$loader = ".loader";

function page_onload() {
    navbutton_init("#nav-button-main", $mainPage);
    navbutton_init("#nav-button-create", $orderCreatePage);
    navbutton_init("#nav-button-orders", $orderListPage);

    if (getCurrentPage() === $orderCreatePage) {
        generatePositions();
    }
}

function navbutton_init(selector, page) {
    var currentPage = getCurrentPage();
    currentPage = currentPage === undefined || currentPage === null
        ? $mainPage
        : currentPage;

    if (currentPage === page) {
        $(selector).addClass("btn-primary");
        $(selector).attr("disabled", "true");
    } else {
        $(selector).addClass("btn-default");
    }

    $(selector).on("click", function() {
        location.href = page;
    });
}

function getOrderPositions() {
    var positions = JSON.parse(sessionStorage.getItem($positionsLitteral));
    if (positions === null || positions === undefined || positions.length <= 0) {
        return [];
    }
    return positions;
};

//rewrite - если true, то значение перезаписываем, иначе добавляем
function addOrderPositions(pizzaId, count, rewrite) {
    count = parseInt(count);
    var newPositions = getOrderPositions();

    for (var position in newPositions) {
        position = newPositions[position];
        if (position.MenuPositionId !== pizzaId) {
            continue;
        }
        rewrite
            ? position.Count = count
            : position.Count += count;
        sessionStorage.setItem($positionsLitteral, JSON.stringify(newPositions));
        return;
    }

    newPositions.push({ MenuPositionId: pizzaId, Count: count });
    sessionStorage.setItem($positionsLitteral, JSON.stringify(newPositions));
}

function setOrderPositions(pizzaId, count) {
    addOrderPositions(pizzaId, count, true);
}

function getCurrentPage() {
    return $("#currentPage").val();
}