﻿using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using PizzaOrdering.Infractructure.AutofacModules;
using WebApiInfrastructureBase.AutofacModules;

namespace PizzaOrdering.Web
{
    public class RegisterConfig
    {
        public static void RegisterContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly())
                .InstancePerRequest();
            builder.RegisterControllers(Assembly.GetExecutingAssembly())
                .InstancePerRequest();
            builder.RegisterFilterProvider();
            builder.RegisterModule(new AutofacWebTypesModule());
            builder.RegisterModule<PizzaOrderingInfrastructurePerRequestModule>();
            builder.RegisterModule(new WebApiInfrastructureBasePerRequestModule());
            var container = builder.Build();

            GlobalConfiguration.Configuration.DependencyResolver =
                new AutofacWebApiDependencyResolver(container);

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}