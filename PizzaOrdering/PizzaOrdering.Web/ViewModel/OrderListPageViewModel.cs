﻿using System.Collections.Generic;
using PizzaOrdering.Infractructure.Model.DataContainer;

namespace PizzaOrdering.Web.ViewModel
{
    public class OrderListPageViewModel : ParentViewModel
    {
        public List<OrderContainer> OrdersList;

        public OrderListPageViewModel(List<OrderContainer> ordersList)
        {
            OrdersList = ordersList;
            Imports.Add("order-list-page.js");
        }
    }
}