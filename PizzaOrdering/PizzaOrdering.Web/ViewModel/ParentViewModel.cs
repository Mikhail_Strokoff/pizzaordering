﻿using System.Collections.Generic;
using System.Linq;

namespace PizzaOrdering.Web.ViewModel
{
    public class ParentViewModel
    {
        protected static Dictionary<string, string> Pages = new Dictionary<string, string>
        {
            {
                "IndexPage",
                "/Home/Index"
            },
            {
                "OrderCreatePage",
                "/Orders/OrderCreate"
            },
            {
                "OrderListPage",
                "/Orders/OrderList"
            },
            {
                "OrderPage",
                "/Orders/Order"
            }
        };

        public List<string> Imports = new List<string>
        {
            "jquery-1.10.2.js",
            "common.js"
        };

        public string CurrentPage => Pages[GetType().Name.Replace("ViewModel", string.Empty)];
    }
}