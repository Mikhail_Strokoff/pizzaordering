﻿using System.Collections.Generic;
using PizzaOrdering.Infractructure.Model.DataContainer;

namespace PizzaOrdering.Web.ViewModel
{
    public class IndexPageViewModel : ParentViewModel
    {
        public List<MenuPositionContainer> MenuPositionsList;

        public IndexPageViewModel(List<MenuPositionContainer> menuPositionsList)
        {
            MenuPositionsList = menuPositionsList;
            Imports.Add("index-page.js");
        }
    }
}