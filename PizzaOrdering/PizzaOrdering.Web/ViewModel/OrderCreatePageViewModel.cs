﻿namespace PizzaOrdering.Web.ViewModel
{
    public class OrderCreatePageViewModel : ParentViewModel
    {
        public OrderCreatePageViewModel()
        {
            Imports.Add("order-create-page.js");
        }
    }
}