﻿using PizzaOrdering.Infractructure.Model.DataContainer;

namespace PizzaOrdering.Web.ViewModel
{
    public class OrderPageViewModel : ParentViewModel
    {
        public OrderContainer Order;

        public OrderPageViewModel(OrderContainer order)
        {
            Order = order;
            Imports.Add("order-page.js");
        }
    }
}