﻿using System.Collections.Generic;
using System.Linq;

namespace PizzaOrdering.Client.Model
{
    public class OrderContainer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public int Entrance { get; set; }
        public string Flat { get; set; }
        public List<OrderPositionContainer> OrderPositions { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public string Address => $"ул. {Street}, д. {House}, кв. {Flat}, п. {Entrance}.";
        public double Total => OrderPositions?.Sum(oPos => oPos?.Total) ?? 0.0;
        public string TotalAsString => Total.ToString("##.## руб");
    }
}