﻿namespace PizzaOrdering.Client.Model
{
    public class OrderPositionContainer
    {
        public int Id { get; set; }
        public int MenuPositionId { get; set; }
        public int Count { get; set; }
        public MenuPositionContainer MenuPosition { get; set; }
        public double Total => MenuPosition?.Price * Count ?? 0.0;
        public string TotalAsString => Total.ToString("##.## руб");
    }
}