﻿namespace PizzaOrdering.Client.Model
{
    public class MenuPositionContainer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public string PriceAsString => Price.ToString("##.## руб");
    }
}