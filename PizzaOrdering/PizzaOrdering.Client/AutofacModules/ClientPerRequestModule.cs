﻿using Autofac;
using AutofacBase;

namespace PizzaOrdering.Client.AutofacModules
{
    public class ClientPerRequestModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var module = new ClientModuleBuilder(builder);
            module.Registers(ModuleType.PerRequest);
        }
    }
}