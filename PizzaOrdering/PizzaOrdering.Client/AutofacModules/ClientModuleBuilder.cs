﻿using Autofac;
using AutofacBase;
using PizzaOrdering.Client.Services;
using PizzaOrdering.Client.Services.Interfaces;

namespace PizzaOrdering.Client.AutofacModules
{
    public class ClientModuleBuilder : AutofacModuleBuilder
    {
        public ClientModuleBuilder(ContainerBuilder builder)
            : base(builder)
        {
        }

        public override void Registers(ModuleType moduleType)
        {
            SimpleRegister<MenuApiService, IMenuApiService>(moduleType);
            SimpleRegister<OrderApiService, IOrderApiService>(moduleType);
        }
    }
}