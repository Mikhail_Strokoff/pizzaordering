﻿using System.Collections.Generic;
using MenuPositionContainer = PizzaOrdering.Client.Model.MenuPositionContainer;

namespace PizzaOrdering.Client.Services.Interfaces
{
    public interface IMenuApiService
    {
        List<MenuPositionContainer> GetMenu();
        MenuPositionContainer GetMenuPosition(int id);
    }
}