﻿using System.Collections.Generic;
using OrderContainer = PizzaOrdering.Client.Model.OrderContainer;

namespace PizzaOrdering.Client.Services.Interfaces
{
    public interface IOrderApiService
    {
        List<OrderContainer> GetOrders();
        OrderContainer GetOrder(int id);
        List<OrderContainer> CreateOrder(OrderContainer order);
        List<OrderContainer> UpdateOrder(OrderContainer order);
        List<OrderContainer> CancelOrder(int id);
    }
}