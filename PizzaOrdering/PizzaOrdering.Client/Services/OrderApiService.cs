﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using PizzaOrdering.Client.Model;
using PizzaOrdering.Client.Services.Interfaces;
using ServiceStack.Text;
using WebApiClientBase;

namespace PizzaOrdering.Client.Services
{
    public class OrderApiService : ClientServiceBase, IOrderApiService
    {
        private const string API = "OrderApi";
        private string BaseUrl => ConfigurationManager.AppSettings["baseUrl"];

        public List<OrderContainer> GetOrders()
        {
            var url = new WebApiUrl(BaseUrl, API);
            return Get<List<OrderContainer>>(url);
        }

        public OrderContainer GetOrder(int id)
        {
            var url = new WebApiUrl(BaseUrl, API);
            return Get<List<OrderContainer>>($"{url}{id}").First();
        }

        public List<OrderContainer> CreateOrder(OrderContainer order)
        {
            ValidateOrderFields(order);

            var url = new WebApiUrl(BaseUrl, API);
            return Post<List<OrderContainer>>(url, order);
        }

        public List<OrderContainer> UpdateOrder(OrderContainer order)
        {
            ValidateOrderFields(order);

            var url = new WebApiUrl(BaseUrl, API);
            return Put<List<OrderContainer>>(url, order);
        }

        public List<OrderContainer> CancelOrder(int id)
        {
            var url = new WebApiUrl(BaseUrl, API);
            return Delete<List<OrderContainer>>($"{url}{id}");
        }

        private static void ValidateOrderFields(OrderContainer order)
        {
            var nullParams =
                $"{(order.OrderPositions.IsNullOrEmpty() ? nameof(order.OrderPositions) : string.Empty)} " +
                $"{(string.IsNullOrWhiteSpace(order.Street) ? nameof(order.Street) : string.Empty)} " +
                $"{(string.IsNullOrWhiteSpace(order.House) ? nameof(order.House) : string.Empty)} " +
                $"{(string.IsNullOrWhiteSpace(order.Flat) ? nameof(order.Flat) : string.Empty)}";

            if (!string.IsNullOrWhiteSpace(nullParams))
                throw new ArgumentNullException(nullParams.Trim());
        }
    }
}