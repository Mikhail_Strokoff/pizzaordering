﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using PizzaOrdering.Client.Model;
using PizzaOrdering.Client.Services.Interfaces;
using WebApiClientBase;

namespace PizzaOrdering.Client.Services
{
    public class MenuApiService : ClientServiceBase, IMenuApiService
    {
        private const string API = "MenuApi";
        private string BaseUrl => ConfigurationManager.AppSettings["baseUrl"];

        public List<MenuPositionContainer> GetMenu()
        {
            var url = new WebApiUrl(BaseUrl, API);
            var result = Get<List<MenuPositionContainer>>(url);
            return result;
        }

        public MenuPositionContainer GetMenuPosition(int id)
        {
            var url = new WebApiUrl(BaseUrl, API);
            var result = Get<List<MenuPositionContainer>>($"{url}{id}").First();
            return result;
        }
    }
}