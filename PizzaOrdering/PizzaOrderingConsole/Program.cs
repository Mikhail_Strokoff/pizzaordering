﻿using PizzaOrdering.Console.CommandHandling;

namespace PizzaOrdering.Console
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var commandListener = new CommandListener();

            commandListener.WaitForCommand();
        }
    }
}