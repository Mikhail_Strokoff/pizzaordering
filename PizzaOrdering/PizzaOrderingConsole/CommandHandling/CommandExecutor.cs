﻿using System;
using System.Collections.Generic;
using System.Linq;
using PizzaOrdering.Client.Model;
using PizzaOrdering.Client.Services.Interfaces;

namespace PizzaOrdering.Console.CommandHandling
{
    internal class CommandExecutor
    {
        private readonly IMenuApiService _menuApiService;
        private readonly IOrderApiService _orderApiService;

        public CommandExecutor(
            IOrderApiService orderService,
            IMenuApiService menuPositionService)
        {
            _orderApiService = orderService;
            _menuApiService = menuPositionService;
        }

        internal List<MenuPositionContainer> GetMenu()
        {
            return _menuApiService.GetMenu();
        }

        internal List<OrderContainer> GetOrders()
        {
            return _orderApiService.GetOrders();
        }

        internal void PrintOrders()
        {
            var orders = GetOrders();
            var ordersOut = "Заказы:";
            orders.ForEach(order =>
            {
                ordersOut += "\r\n";
                ordersOut += $"\r\n{order.Id} - {order.Address}";
                var i = 1;
                if (order.OrderPositions?.Any() ?? false)
                    order.OrderPositions.ToList().ForEach(
                        oPos =>
                        {
                            ordersOut +=
                                $"\r\n - {i++} - {oPos.MenuPosition?.Name ?? string.Empty} - {oPos.Count}";
                        });
            });
            WriteLine(ordersOut);
        }

        internal void PrintMenu()
        {
            var menu = GetMenu();
            var menuOut = "Меню:";
            menu.ForEach(item => { menuOut += $"\r\n - {item.Id} - {item.Name}"; });
            WriteLine(menuOut);
        }

        internal void CreateOrder(OrderContainer order)
        {
            try
            {
                _orderApiService.CreateOrder(order);
            }
            catch (ArgumentNullException e)
            {
                WriteLine(e.Message);
            }
        }

        internal void CancelOrder(int id)
        {
            _orderApiService.CancelOrder(id);
        }

        internal OrderPositionContainer ParsePosition(string input)
        {
            try
            {
                var index = input.IndexOf("-", StringComparison.Ordinal);
                var menuPositionId = int.Parse(input.Substring(0, index).Trim());
                var count = int.Parse(input.Substring(index + 1));
                return new OrderPositionContainer
                {
                    MenuPositionId = menuPositionId,
                    Count = count
                };
            }
            catch (Exception e)
            {
                WriteLine(e.Message);
                return null;
            }
        }

        internal static OrderContainer ParseOrder(string address)
        {
            address = address
                .Replace("ул.", "")
                .Replace("кв.", "")
                .Replace("д.", "")
                .Replace("п.", "")
                .Replace(".", "");
            var indexStart = 0;
            var addressParts = new string[4];
            for (var i = 0; i < 4 && indexStart < address.Length; i++)
            {
                var indexEnd = address.IndexOf(',', indexStart);
                indexEnd = indexEnd < 0 ? address.Length : indexEnd;
                addressParts[i] = address.Substring(indexStart, indexEnd - indexStart).Trim();
                indexStart = indexEnd + 1;
            }
            int.TryParse(addressParts[3], out indexStart);

            return new OrderContainer
            {
                Street = addressParts[0],
                House = addressParts[1],
                Flat = addressParts[2],
                Entrance = indexStart,
                OrderPositions = new List<OrderPositionContainer>()
            };
        }

        internal static void WriteLine(string text, bool before = true, bool after = true)
        {
            if (before) System.Console.WriteLine();
            System.Console.WriteLine(text);
            if (after) System.Console.WriteLine();
        }

        internal static string ReadLine(string text, bool before = true)
        {
            if (before) System.Console.WriteLine();
            System.Console.Write(text);
            return System.Console.ReadLine();
        }
    }
}