﻿using Autofac;
using PizzaOrdering.Client.AutofacModules;
using PizzaOrdering.Client.Services.Interfaces;
using static PizzaOrdering.Console.CommandHandling.CommandExecutor;

namespace PizzaOrdering.Console.CommandHandling
{
    internal class CommandListener
    {
        private CommandExecutor _commandExecutor;

        public void WaitForCommand()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<ClientPerLifetimeScopeModule>();
            var container = builder.Build();

            using (var scope = container.BeginLifetimeScope())
            {
                _commandExecutor = new CommandExecutor(
                    scope.Resolve<IOrderApiService>(),
                    scope.Resolve<IMenuApiService>()
                    );

                System.Console.WriteLine(HELLO_TEXT);

                while (true)
                {
                    WriteLine(HELP_TEXT);
                    var command = ReadLine(string.Empty);
                    switch (command)
                    {
                        case "1": // Print menu
                            _commandExecutor.PrintMenu();
                            break;
                        case "2": // Add order
                            AddOrder();
                            break;
                        case "3": // Show orders
                            _commandExecutor.PrintOrders();
                            break;
                        case "4": // Cancel order
                            _commandExecutor.CancelOrder(int.Parse(ReadLine("Введите номер заказа: ")));
                            break;
                        case "0": // Exit
                            return;
                        default:
                            WriteLine(WRONG_INPUT);
                            break;
                    }
                }
            }
        }

        private void AddOrder()
        {
            var order = ParseOrder(ReadLine("Введите адрес доставки (ул. [Улица], д. [Дом], кв. [Квартира], п. [Подъезд]): "));

            while (true)
            {
                WriteLine(ADD_ORDER_TEXT);
                switch (System.Console.ReadLine())
                {
                    case "1": // Print menu
                        _commandExecutor.PrintMenu();
                        break;
                    case "2": // Add position
                    {
                        var newPosition =
                            _commandExecutor.ParsePosition(ReadLine("Введите заказ в формате [номер пиццы - количество]: "));
                        if (newPosition == null)
                            WriteLine(WRONG_INPUT);
                        else
                            order.OrderPositions.Add(newPosition);
                    }
                        break;
                    case "3": // Cancel order
                        return;
                    case "4": // Accept order
                        _commandExecutor.CreateOrder(order);
                        return;
                    default:
                        WriteLine(WRONG_INPUT);
                        break;
                }
            }
        }

        #region Text

        private const string HELLO_TEXT = "Добро пожаловать в приложение для заказа пиццы ! ";

        private const string HELP_TEXT =
            "Выберите команду и введите её номер:" +
            "\r\n 1: показать ассортимент;" +
            "\r\n 2: добавить новый заказ;" +
            "\r\n 3: показать активные заказы;" +
            "\r\n 4: отменить заказ;" +
            "\r\n 0: Закрыть приложение.";

        private const string ADD_ORDER_TEXT =
            "Выберите команду и введите её номер:" +
            "\r\n 1: показать ассортимент;" +
            "\r\n 2: добавить позицию в заказ;" +
            "\r\n 3: отменить текущий заказ;" +
            "\r\n 4: подтвердить заказ.";

        private const string WRONG_INPUT = "Wrong input! ";

        #endregion
    }
}