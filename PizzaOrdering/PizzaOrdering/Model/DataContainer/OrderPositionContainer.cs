﻿using PizzaOrdering.Infractructure.Model.Data;

namespace PizzaOrdering.Infractructure.Model.DataContainer
{
    public class OrderPositionContainer
    {
        public OrderPositionContainer(OrderPositionDomain orderPosition)
        {
            Id = orderPosition.Id;
            MenuPositionId = orderPosition.MenuPositionId;
            Count = orderPosition.Count;
            MenuPosition = new MenuPositionContainer(orderPosition.MenuPositionDomain);
        }

        public OrderPositionContainer()
        {
        }

        public int Id { get; set; }
        public int MenuPositionId { get; set; }
        public int Count { get; set; }
        public MenuPositionContainer MenuPosition { get; set; }
        public double Total => MenuPosition?.Price * Count ?? 0.0;
        public string TotalAsString => Total.ToString("##.## руб");
    }
}