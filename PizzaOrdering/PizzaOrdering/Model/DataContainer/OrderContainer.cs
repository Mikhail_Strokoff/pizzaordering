﻿using System.Collections.Generic;
using System.Linq;
using PizzaOrdering.Infractructure.Model.Data;

namespace PizzaOrdering.Infractructure.Model.DataContainer
{
    public class OrderContainer
    {
        public OrderContainer(OrderDomain order)
        {
            Id = order.Id;
            FirstName = order.FirstName;
            LastName = order.LastName;
            Street = order.Street;
            House = order.House;
            Entrance = order.Entrance;
            Flat = order.Flat;
            OrderPositions = order.OrderPositions
                .Select(oPos => new OrderPositionContainer(oPos))
                .ToList();
        }

        public OrderContainer()
        {
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public int Entrance { get; set; }
        public string Flat { get; set; }
        public List<OrderPositionContainer> OrderPositions { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public string Address => $"ул. {Street}, д. {House}, кв. {Flat}, п. {Entrance}.";
        public double Total => OrderPositions?.Sum(oPos => oPos?.Total) ?? 0.0;
        public string TotalAsString => Total.ToString("##.## руб");
    }
}