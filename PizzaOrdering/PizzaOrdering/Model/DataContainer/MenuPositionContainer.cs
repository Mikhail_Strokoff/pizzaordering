﻿using PizzaOrdering.Infractructure.Model.Data;

namespace PizzaOrdering.Infractructure.Model.DataContainer
{
    public class MenuPositionContainer
    {
        public MenuPositionContainer(MenuPositionDomain menuPosition)
        {
            Id = menuPosition.Id;
            Name = menuPosition.Name;
            Description = menuPosition.Description;
            Price = menuPosition.Price;
        }
        public MenuPositionContainer()
        {
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public string PriceAsString => Price.ToString("##.## руб");
        public string PizzaImg => $"/Content/pages/img/pizza{Id}.jpg";
    }
}