﻿using InfrastructureBase.Data;

namespace PizzaOrdering.Infractructure.Model.Data
{
    public class OrderPositionDomain : BaseDomain
    {
        public int Id { get; set; }
        public int MenuPositionId { get; set; }
        public int OrderId { get; set; }
        public int Count { get; set; }
        public virtual MenuPositionDomain MenuPositionDomain { get; set; }
        public virtual OrderDomain OrderDomain { get; set; }
    }
}