﻿using System.Collections.Generic;
using InfrastructureBase.Data;

namespace PizzaOrdering.Infractructure.Model.Data
{
    public class OrderDomain : BaseDomain
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public int Entrance { get; set; }
        public string Flat { get; set; }
        public virtual ICollection<OrderPositionDomain> OrderPositions { get; set; }
    }
}