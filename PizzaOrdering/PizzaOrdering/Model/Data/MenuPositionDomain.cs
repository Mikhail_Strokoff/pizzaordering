﻿using System.Collections.Generic;
using InfrastructureBase.Data;

namespace PizzaOrdering.Infractructure.Model.Data
{
    public class MenuPositionDomain : BaseDomain
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public virtual ICollection<OrderPositionDomain> OrderPositions { get; set; }
    }
}