﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Reflection;
using InfrastructureBase.Data;

namespace PizzaOrdering.Infractructure.Model
{
    public class PizzaDbContext : DbContext, IDbContext
    {
        public PizzaDbContext()
        {
            AddAspNetLogin();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
                .Where(type => !string.IsNullOrEmpty(type.Namespace))
                .Where(type => type.BaseType != null && type.BaseType.IsGenericType
                               && type.BaseType.GetGenericTypeDefinition() == typeof (EntityTypeConfiguration<>));
            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<TEntity> Set<TEntity>() where TEntity : BaseDomain
        {
            return base.Set<TEntity>();
        }

        public void AddAspNetLogin()
        {
            Database.ExecuteSqlCommand(
                "IF NOT EXISTS (SELECT * FROM master.sys.syslogins WHERE name = 'IIS APPPOOL\\ASP.NET v4.0') " +
                "CREATE LOGIN [IIS APPPOOL\\ASP.NET v4.0] FROM WINDOWS " +
                "GRANT CONTROL TO [IIS APPPOOL\\ASP.NET v4.0]");
            SaveChanges();
        }
    }
}