﻿using System.Data.Entity.ModelConfiguration;
using PizzaOrdering.Infractructure.Model.Data;

namespace PizzaOrdering.Infractructure.Mapping
{
    internal class MenuPositionDomainMap : EntityTypeConfiguration<MenuPositionDomain>
    {
        public MenuPositionDomainMap()
        {
            ToTable("pizza_menu_position");
            HasKey(mPos => mPos.Id);
            Property(mPos => mPos.Name).IsRequired().HasMaxLength(100);
            Property(mPos => mPos.Description).IsOptional();
            Property(mPos => mPos.Price).IsRequired();
        }
    }
}