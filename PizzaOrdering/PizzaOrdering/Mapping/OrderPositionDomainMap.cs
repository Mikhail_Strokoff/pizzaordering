﻿using System.Data.Entity.ModelConfiguration;
using PizzaOrdering.Infractructure.Model.Data;

namespace PizzaOrdering.Infractructure.Mapping
{
    public class OrderPositionDomainMap : EntityTypeConfiguration<OrderPositionDomain>
    {
        public OrderPositionDomainMap()
        {
            ToTable("pizza_order_position");
            HasKey(oPos => oPos.Id);
            Property(oPos => oPos.MenuPositionId).IsRequired().HasColumnName("MenuPositionDomainId");
            Property(oPos => oPos.OrderId).IsRequired().HasColumnName("OrderDomainId");
            Property(oPos => oPos.Count).IsRequired();

            HasRequired(oPos => oPos.OrderDomain).WithMany(order => order.OrderPositions).HasForeignKey(oPos => oPos.OrderId);
            HasRequired(oPos => oPos.MenuPositionDomain).WithMany(mPos => mPos.OrderPositions).HasForeignKey(oPos => oPos.MenuPositionId);
        }
    }
}