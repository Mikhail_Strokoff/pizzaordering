﻿using System.Data.Entity.ModelConfiguration;
using PizzaOrdering.Infractructure.Model.Data;

namespace PizzaOrdering.Infractructure.Mapping
{
    public class OrderDomainMap : EntityTypeConfiguration<OrderDomain>
    {
        public OrderDomainMap()
        {
            ToTable("pizza_order");
            HasKey(order => order.Id);
            Property(order => order.FirstName).IsOptional().HasMaxLength(20);
            Property(order => order.LastName).IsOptional().HasMaxLength(30);
            Property(order => order.Street).IsRequired().HasMaxLength(30);
            Property(order => order.House).IsRequired().HasMaxLength(10);
            Property(order => order.Entrance).IsOptional();
            Property(order => order.Flat).IsRequired().HasMaxLength(5);
        }
    }
}