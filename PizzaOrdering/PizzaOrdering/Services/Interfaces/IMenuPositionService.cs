﻿using System.Collections.Generic;
using PizzaOrdering.Infractructure.Model.Data;

namespace PizzaOrdering.Infractructure.Services.Interfaces
{
    public interface IMenuPositionService
    {
        List<MenuPositionDomain> GetMenu();
        MenuPositionDomain GetMenuPosition(int id);
    }
}