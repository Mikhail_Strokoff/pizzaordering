﻿using System.Collections.Generic;
using PizzaOrdering.Infractructure.Model.Data;
using PizzaOrdering.Infractructure.Model.DataContainer;

namespace PizzaOrdering.Infractructure.Services.Interfaces
{
    public interface IOrderService
    {
        List<OrderDomain> GetOrders();
        int CreateOrder(OrderContainer order);
        void CancelOrder(int id);
        OrderDomain GetOrderById(int id);
        OrderDomain UpdateOrder(OrderContainer order);
    }
}