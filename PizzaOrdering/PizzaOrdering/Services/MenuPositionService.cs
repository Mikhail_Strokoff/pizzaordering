﻿using System.Collections.Generic;
using System.Linq;
using InfrastructureBase.Data;
using PizzaOrdering.Infractructure.Model.Data;
using PizzaOrdering.Infractructure.Services.Interfaces;

namespace PizzaOrdering.Infractructure.Services
{
    public class MenuPositionService : IMenuPositionService
    {
        private readonly IRepository<MenuPositionDomain> _menuPositionRepository;

        public MenuPositionService(IRepository<MenuPositionDomain> menuPositionRepository)
        {
            _menuPositionRepository = menuPositionRepository;
        }

        public List<MenuPositionDomain> GetMenu()
        {
            return _menuPositionRepository.TableNoTracking.ToList();
        }

        public MenuPositionDomain GetMenuPosition(int id)
        {
            return _menuPositionRepository.GetById(id);
        }
    }
}