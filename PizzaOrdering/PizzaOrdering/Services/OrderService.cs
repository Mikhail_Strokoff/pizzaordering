﻿using System.Collections.Generic;
using System.Linq;
using InfrastructureBase.Data;
using PizzaOrdering.Infractructure.Model.Data;
using PizzaOrdering.Infractructure.Model.DataContainer;
using PizzaOrdering.Infractructure.Services.Interfaces;

namespace PizzaOrdering.Infractructure.Services
{
    public class OrderService : IOrderService
    {
        private readonly IRepository<OrderPositionDomain> _orderPositionRepository;
        private readonly IRepository<OrderDomain> _orderRepository;

        public OrderService(
            IRepository<OrderDomain> orderRepository,
            IRepository<OrderPositionDomain> orderPositionRepository)
        {
            _orderRepository = orderRepository;
            _orderPositionRepository = orderPositionRepository;
        }

        public List<OrderDomain> GetOrders()
        {
            return _orderRepository.TableNoTracking.ToList();
        }

        public OrderDomain GetOrderById(int id)
        {
            return _orderRepository.GetById(id);
        }

        public OrderDomain UpdateOrder(OrderContainer order)
        {
            var orderDomain = _orderRepository.GetById(order.Id);
            orderDomain.FirstName = order.FirstName;
            orderDomain.LastName = order.LastName;
            orderDomain.Street = order.Street;
            orderDomain.House = order.House;
            orderDomain.Entrance = order.Entrance;
            orderDomain.Flat = order.Flat;

            _orderPositionRepository.DeleteRange(orderDomain.OrderPositions);
            orderDomain.OrderPositions = order.OrderPositions.Select(oPos =>
                new OrderPositionDomain
                {
                    MenuPositionId = oPos.MenuPosition.Id,
                    Count = oPos.Count,
                    OrderId = order.Id
                }).ToList();
            _orderRepository.Update(orderDomain);
            return orderDomain;
        }

        public int CreateOrder(OrderContainer order)
        {
            var orderDomain = new OrderDomain
            {
                FirstName = order.FirstName,
                LastName = order.LastName,
                Street = order.Street,
                House = order.House,
                Entrance = order.Entrance,
                Flat = order.Flat,
                OrderPositions = order.OrderPositions.Select(oPos => new OrderPositionDomain
                {
                    MenuPositionId = oPos.MenuPosition.Id,
                    Count = oPos.Count
                }).ToList()
            };
            _orderRepository.Insert(orderDomain);
            return orderDomain.Id;
        }

        public void CancelOrder(int id)
        {
            var order = _orderRepository.GetById(id);
            _orderPositionRepository.DeleteRange(order.OrderPositions);
            _orderRepository.Delete(order);
        }
    }
}