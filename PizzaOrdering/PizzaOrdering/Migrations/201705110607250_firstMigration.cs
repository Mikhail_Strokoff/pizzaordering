using System.Data.Entity.Migrations;

namespace PizzaOrdering.Infractructure.Migrations
{
    public partial class firstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.pizza_menu_position",
                c => new
                {
                    Id = c.Int(false, true),
                    Name = c.String(false, 100),
                    Description = c.String(),
                    Price = c.Double(false)
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.pizza_order_position",
                c => new
                {
                    Id = c.Int(false, true),
                    MenuPositionDomainId = c.Int(false),
                    OrderDomainId = c.Int(false),
                    Count = c.Int(false)
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.pizza_menu_position", t => t.MenuPositionDomainId, true)
                .ForeignKey("dbo.pizza_order", t => t.OrderDomainId, true)
                .Index(t => t.MenuPositionDomainId)
                .Index(t => t.OrderDomainId);

            CreateTable(
                "dbo.pizza_order",
                c => new
                {
                    Id = c.Int(false, true),
                    FirstName = c.String(maxLength: 20),
                    LastName = c.String(maxLength: 30),
                    Street = c.String(false, 30),
                    House = c.String(false, 10),
                    Entrance = c.Int(),
                    Flat = c.String(false, 5)
                })
                .PrimaryKey(t => t.Id);
        }

        public override void Down()
        {
            DropForeignKey("dbo.pizza_order_position", "OrderDomainId", "dbo.pizza_order");
            DropForeignKey("dbo.pizza_order_position", "MenuPositionDomainId", "dbo.pizza_menu_position");
            DropIndex("dbo.pizza_order_position", new[] {"OrderDomainId"});
            DropIndex("dbo.pizza_order_position", new[] {"MenuPositionDomainId"});
            DropTable("dbo.pizza_order");
            DropTable("dbo.pizza_order_position");
            DropTable("dbo.pizza_menu_position");
        }
    }
}