﻿using Autofac;
using AutofacBase;

namespace PizzaOrdering.Infractructure.AutofacModules
{
    public class PizzaOrderingInfrastructurePerLifetimeScopeModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var module = new PizzaOrderingInfrastructureModuleBuilder(builder);
            module.Registers(ModuleType.PerLifetimeScope);
        }
    }
}