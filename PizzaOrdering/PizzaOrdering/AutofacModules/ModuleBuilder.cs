﻿using Autofac;
using AutofacBase;
using InfrastructureBase.Data;
using PizzaOrdering.Infractructure.Model;
using PizzaOrdering.Infractructure.Services;
using PizzaOrdering.Infractructure.Services.Interfaces;

namespace PizzaOrdering.Infractructure.AutofacModules
{
    public class PizzaOrderingInfrastructureModuleBuilder : AutofacModuleBuilder
    {
        public PizzaOrderingInfrastructureModuleBuilder(ContainerBuilder builder)
            : base(builder)
        {
        }

        public override void Registers(ModuleType moduleType)
        {
            SimpleRegister<MenuPositionService, IMenuPositionService>(moduleType);
            SimpleRegister<OrderService, IOrderService>(moduleType);

            SimpleRegister<PizzaDbContext, IDbContext>(moduleType);

            RegisterGeneric(typeof (EfRepository<>), typeof (IRepository<>), moduleType);
        }
    }
}